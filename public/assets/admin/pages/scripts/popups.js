

$(document).ready(function () {
    $('.popup').magnificPopup({
        type: 'iframe',
        callbacks: {
            open: function () {
                // Will fire when this exact popup is opened
                // this - is Magnific Popup object
            },
            close: function () {
                    var url = document.URL;
                    var goTo = url.replace("#", "");
                    pjaxPage(goTo);
                    
                }
                // e.t.c.
        }

    });
});

$(document).on('ready pjax:success', function () {
    $('.gallery-item').magnificPopup({
      type: 'iframe',
    
      gallery:{
        enabled:true
      },
        iframe: {
        markup: '<div style="width:1200px !important; height:100%;">'+
                '<div class="mfp-iframe-scaler" >'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div></div>'
    },
        callbacks: {
    elementParse: function(item) {
      // Function will fire for each target element
      // "item.el" is a target DOM element (if present)
      // "item.src" is a source that you may modify
        
      if(item.el.context.host == 'www.youtube.com') {
     item.type = 'iframe';
} else {
    item.type = 'iframe';
 }

    }
  }
        
    });
    $('.popup').magnificPopup({
        type: 'iframe',
        callbacks: {
            open: function () {
                // Will fire when this exact popup is opened
                // this - is Magnific Popup object
            },
            close: function () {
                    var url = document.URL;
                    var goTo = url.replace("#", ""); 
                    pjaxPage(goTo);
                }
                // e.t.c.
        }

    });
});
