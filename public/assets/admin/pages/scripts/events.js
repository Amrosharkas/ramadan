$(document).on('ready pjax:success' , function () {
	$(document).on('click', '.refresh_video', function (e) {
    e.preventDefault();
    $a_tag = $(this);
    var file_id = $(this).data('id');
    
    $.ajax({
        url: "/files/get_file_info",
        method: "post",
        data:{
            id:file_id
        },
        success: function (data) {
            if (data.processed == 1){
                $a_tag.attr("href",data.href); 
                 
                $a_tag.find("img").attr("src",data.img);
                $a_tag.removeClass("refresh_video");
            }else{
                toastr['info']('Video Processing', "Video is processing please wait");
            }
        },
    })
});

});
