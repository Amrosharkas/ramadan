// prepare the form when the DOM is ready 
$(document).on('ready pjax:success', function () {


    //Hidden elements Dependencies
    $dependent_emements = $(".dependent");


    $dependent_emements.each(function () {
        var depending_on_name = $(this).attr("data-depending-on");
        var depending_on_value = $(this).attr("data-depending-value");

        $depending_on_element = $("[name=" + depending_on_name + "]");
        $depending_on_element.addClass("master_dependency");
        if ($depending_on_element.data('depend') != depending_on_value) {
           
            // $(this).find("input,select").val("");
            $(this).find("input,select").change();
              $(this).find("input,select").hide();
            $(this).hide(250);

        } else {
            //$(this).find(".timepicker").val("Choose Time");
            
            $(this).show(250);
        }

    });

    $(".master_dependency").change(function () {
        var element_name = $(this).attr("name");
        var element_value = $(this).find(':selected').data('depend');
        if(element_value == null && $(this).attr("type") == 'radio'){//input radio
            element_value=$(this).data('depend')
            // console.log(element_value)
        }
        if(element_value==null){ // input text
            if ($(this).val() != null && $(this).val() != 0){
                element_value = 1;
                // console.log($(this).val());
            } else {
                element_value =0;
                // console.log(0);
            }
        }
        $dependent_element = $('*[data-depending-on="' + element_name + '"]');
        $dependent_element.each(function () {
            var showValue = $(this).attr("data-depending-value");

            if (showValue == element_value) {
                // console.log(element_value);
                $(this).find("input,select").show();
                $(this).show(250);
                // $(this).find(".timepicker").val("Choose Time");
            } else {
                // console.log(element_value);
                $(this).hide(250);
                // $(this).find("input,select").val("");
                $(this).find("input,select").hide();
                $(this).find("input,select").change();
            }
        });
    });
    // Hidden options dependencies

    $dependent_emements = $(".options_dependent");


    $dependent_emements.each(function () {
        var depending_on_name = $(this).attr("data-depending-on");
        //var depending_on_value = $(this).attr("data-depending-value");

        $depending_on_element = $("[name=" + depending_on_name + "]");
        $depending_on_element.addClass("master_options_dependency");
        var masterValue = $depending_on_element.val();
        $dependent_emements.find("option").each(function () {
            var iteratingOkVal = $(this).attr("data-depending-value"); 
            var selectedValue = $(this).parent('select').val();
            if (iteratingOkVal == masterValue  || iteratingOkVal=="force_show" || $(this).attr("value") == "") {
                $(this).show();
                if($(this).parent('select').hasClass("select2")){
                    $(this).removeAttr("disabled");  
                    $(this).parent('select').select2();
                }
            }else{
                $(this).hide();
                if($(this).parent('select').hasClass("select2")){
                    $(this).attr("disabled","disabled");
                    $(this).parent('select').select2();
                    
                }
                if(selectedValue ==  $(this).attr("value")){
                    $(this).parent('select').val("");
                }
            }
        });
        

    });

    $(".master_options_dependency").change(function () {
        var element_name = $(this).attr("name");
        var element_value = $(this).val();

        $dependent_element = $('*[data-depending-on="' + element_name + '"]');
        $dependent_element.find("option").each(function () {
            
            var iteratingOkVal = $(this).attr("data-depending-value"); 
            
            if (iteratingOkVal == element_value || iteratingOkVal=="force_show" || $(this).attr("value") == "") {
                
                
                $(this).show();  
                if($(this).parent('select').hasClass("select2")){
                    $(this).removeAttr("disabled");  
                    $(this).parent('select').select2();
                }
            }else{
                $(this).hide();
                
                if($(this).parent('select').hasClass("select2")){
                    $(this).attr("disabled","disabled");
                    $(this).parent('select').select2("val","");
                    $(this).parent('select').select2();
                    
                }
                $(this).parent('select').val("");
                
            }
        });
        
        
        

    });

    // hide error on changing
    $("input,select").change(function () {
        $(this).removeClass("errorValidation");
        $(this).closest('div').removeClass('error-view');
        $(this).next(".error_message").hide();
    });
    $(".select2").select2({
        placeholder: "",
        allowClear: true
    });
    var options = {
        beforeSubmit: beforeSubmit, // pre-submit callback
        success: success, // post-submit callback
        error: error
    };
    // bind form using 'ajaxForm'
    $('.ajax_form').ajaxForm(options);

});

// pre-submit callback
function beforeSubmit(formData, jqForm, options) {
   appBlockUI();

    // formData is an array; here we use $.param to convert it to a string to display it
    // but the form plugin does this for you automatically when it submits the data
    var queryString = $.param(formData);
    var formElement = jqForm[0];
    var pass = 1;
    $(".error_message").hide();
    if(JSON.stringify(formData).includes('publish')|| JSON.stringify(formData).includes('confirm') || JSON.stringify(formData).includes('modify')) {

        for (var i = 0; i < formData.length; i++) {

            var elementName = formData[i].name.replace("[]", "");
            if (formData[i].type == "select-one") {
                $formElement = $('select[name=' + elementName + ']');
            } else if(formData[i].type == "textarea"){
                $formElement = $('textarea[name=' + elementName + ']');
            } else {
                $formElement = $('input[name=' + elementName + ']');
            }
            $formElement.closest('div').removeClass('error-view');
            $formElement.removeClass("errorValidation");

            validationRules = $formElement.attr('data-validation');

            validationName = $formElement.attr('data-name');
            var check_dependency = true;

            if(! validationRules && ! validationName){ // works with array of inputs
                if (formData[i].type == "select-one") {
                    $formElement = $("select[name='" + formData[i].name  + "']");
                } else {
                    $formElement = $("input[name='" + formData[i].name + "']");
                }

                for (key = 1 ; key < $formElement.length ; key++ ){
                    $input = $($formElement[key-1])
                    validationRules = $input.attr('data-validation');
                    validationName = $input.attr('data-name');
                    validationCount = $input.attr('data-count');

                    pass = validateAll(validationRules , check_dependency  , $input , pass)
                    console.log($input)
                    console.log(pass)
                }


            } else { //works with normal (non array) inputs
                $input = $("input[name='" + formData[i].name + "']");
                if($input.length ==0){//for <select>
                    $input = $("select[name='" + formData[i].name + "'");

                }
                pass = validateAll(validationRules , check_dependency  , $input , pass)

            }
            if ($formElement.closest('.dependent').length != 0) {
                if ($formElement.css('display').toLowerCase() == 'none') {
                    check_dependency = false;
                }
            }
        }

    }
    if(JSON.stringify(formData).includes('report')){
        pass =1;
    }

    if (pass == 1) {
        return true;
    } else {

        return false;
    }
}

// post-submit callback 
function success(responseText, statusText, xhr, $form) {
    appUnBlockUI();
     //   App.unblockUI();

    var response = responseText;
    // console.log(response);
    if (response.status == "error") {
        var $toast = toastr["error"](response.msg, "Sorry");
    } else {
        var $toast = toastr["success"](response.msg, "Success");
        if (response.page == "none") {
            $("#reset_button").click();
            $new_comment = $(".clone_comment").clone();
            $new_comment.css("display","block").removeClass("clone_comment");
            $new_comment.html(function(index,html){
                return html.replace("--name--",response.name);
            });
            $new_comment.html(function(index,html){
                return html.replace("--time--",response.time);
            });
            $new_comment.html(function(index,html){
                return html.replace("--comment--",response.comment);
            });
            $new_comment.insertBefore("#comment_container .comment_div:first");
            $("#comments_count").html(response.total_comments);
        } else {
            // console.log(JSON.stringify(response));
             pjaxPage(response.url);
        }
    }
}

function error(responseText, statusText, xhr, $form) {
   appUnBlockUI();
//         App.unblockUI();
    
    var $toast = toastr["error"](responseText, statusText);

}

function validateAll(validationRules ,  check_dependency , $Input , pass ) {


    if (typeof validationRules !== 'undefined' && check_dependency) {
        if (validationRules.includes("match")) {

            var split = validationRules.split(".");
            var field = split[1];
            var fieldValue = $('input[name=' + field + ']').val();
            if ($Input.val() !== fieldValue) {
                appUnBlockUI();
                // App.unblockUI();

                //var $toast = toastr["error"]("Error", validationName+" is required");
                $Input.addClass("errorValidation");
                $Input.closest('div').addClass('error-view');
                $Input.next(".error_message").show().html(field + "s don't match");
                pass = 0;
            }
        }
        if ($Input.val() == "Choose Time" && $Input.hasClass("timepicker")) {
            appUnBlockUI();
            // App.unblockUI();

            //var $toast = toastr["error"]("Error", " Invalid Email");
            $Input.addClass("errorValidation");
            $Input.closest('div').addClass('error-view');
            $Input.next(".error_message").show().html("Invalid Time");
            pass = 0;
        }

        if (!validateEmail($Input.val()) && validationRules.includes("email")) {
            appUnBlockUI();
            // App.unblockUI();

            //var $toast = toastr["error"]("Error", " Invalid Email");
            $Input.addClass("errorValidation");
            $Input.closest('div').addClass('error-view');
            $Input.next(".error_message").show().html("Invalid Email");
            pass = 0;
        }

        if (isNaN($Input.val()) && validationRules.includes("number")) {
            appUnBlockUI();
            // App.unblockUI();

            //var $toast = toastr["error"]("Error", validationName+" must be a number");
            $Input.addClass("errorValidation");
            $Input.closest('div').addClass('error-view');
            $Input.next(".error_message").show().html(validationName + " must be a number");
            pass = 0;
        }

        if ($Input.val() == "" && validationRules.includes("required")) {
            App.unblockUI();
            $Input.addClass("errorValidation");
            $Input.closest('div').addClass('error-view');
            $Input.next(".error_message").show().html(validationName + " is required");
            //var $toast = toastr["error"]("Error", validationName+" is required");
            pass = 0;
        }
        if (!validateURL($Input.val()) && validationRules.includes("url")) {
            appUnBlockUI();
            // App.unblockUI();
            $Input.addClass("errorValidation");
            $Input.closest('div').addClass('error-view');
            $Input.next(".error_message").show().html("Invalid URL");
            pass = 0;
        }
        if (!validateSkypeName($Input.val()) && validationRules.includes("skypeName")) {
            appUnBlockUI();
            // App.unblockUI();
            $Input.addClass("errorValidation");
            $Input.closest('div').addClass('error-view');
            $Input.next(".error_message").show().html("Invalid skype name");
            pass = 0;
        }

        return pass;
    }
    return pass;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validateURL(url) {
    var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
    return urlregex.test(url);
}
function validateSkypeName(skypeName) {
    var regex = /([a-zA-Z][a-zA-Z0-9_\-\,\.]{5,31})$/;
    return regex.test(skypeName)
}
