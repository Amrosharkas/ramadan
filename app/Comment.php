<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Comment extends Model
{
    protected $table='comments';
    protected $fillable=[ 'event_id', 'task_id', 'comment',];
    
 public function event(){
        return $this->belongsTo('App\Event' ,  'event_id');
 }
 public function user(){
        return $this->belongsTo('App\User' ,  'user_id');
 }
 public function task(){
        return $this->belongsTo('App\Task' ,  'task_id');
 }
}
