<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  EventUser extends Model
{
    protected $table='event_users';
    protected $fillable=[ 'event_id', 'user_id', 'owner', 'participant', 'orgnizer', 'invited_by', 'status',];
    
 public function event(){
        return $this->hasOne('App\Event' , 'id','event_id');
 }
 public function user(){
        return $this->hasOne('App\User' , 'id','user_id');
 }
 public function getInviter(){
        return $this->hasOne('App\User' , 'id','invited_by');
 }
}
