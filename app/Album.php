<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Album extends Model
{
    protected $table='albums';
    protected $fillable=[ 'event_id', 'user_id', 'name',];
    
 public function event(){
        return $this->belongsTo('App\Event' ,  'event_id');
 }
 public function user(){
        return $this->belongsTo('App\User' ,  'user_id');
 }
}
