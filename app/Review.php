<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Review extends Model
{
    protected $table='reviews';
    protected $fillable=[ 'review', 'reviewed_by', 'created_by', 'event_id', 'rating',];
    
 public function event(){
        return $this->belongsTo('App\Event' ,  'event_id');
 }
}
