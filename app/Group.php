<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Group extends Model
{
    protected $table='groups';
    protected $fillable=[ 'name', 'created_by', 'users',];
    
public function User(){
        return $this->belongsToMany('App\User' );
    }

public function getCreator(){
        return $this->belongsTo('App\User' ,  'created_by');
 }
}
