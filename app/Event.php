<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Event extends Model
{
    protected $table='events';
    protected $fillable=[ 'name', 'location', 'latitude', 'longitude', 'cost', 'reminders', 'event_image', 'description', 'end_time', 'start_time',];
    
 public function Detail(){
        return $this->belongsToMany('App\Detail' );
    }
    public function PoolDishes(){
        return $this->hasMany('App\Task' )->whereNull('tasks.user_id')->orderBy('category');
    }
    public function AssignedDishes(){
        return $this->hasMany('App\Task' )->whereNotNull('tasks.user_id');
    }
 public function AcceptedUsers(){
        return $this->belongsToMany('App\User','event_users' )->where('event_users.status','Accepted');
    }
    public function PendingUsers(){
        return $this->belongsToMany('App\User','event_users' )->where('event_users.status','Pending');
    }
}
