<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  AbuseCase extends Model
{
    protected $table='abuse_cases';
    protected $fillable=[ 'reported_by', 'status', 'reviewed_by', 'description', 'user_reported', 'file_id', 'comment_id',];
    
 public function file(){
        return $this->belongsTo('App\File' ,  'file_id');
 }
 public function comment(){
        return $this->belongsTo('App\Comment' ,  'comment_id');
 }
}
