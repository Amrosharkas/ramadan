<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Task;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Schema;
use App\User;
use App\Event;
use App\EventUser;
use App\Http\Helpers\Helpers;
use Auth;
use DB;
use App\Comment;
//uses
class TaskController extends Controller {


    public function dishesIndex() {
        $data = [];
        //@@some-data@@
        $data['partialView'] = 'Task.dishes';
        $data['tasks'] = Task::orderBy('created_at', 'asc')->where('admin_show',1)->get();
        $data['pending_tasks'] = Task::whereNotNull('user_id')
        ->whereNull('confirmed')
        
        ->get();
        return view('Task.base', $data);
    }
    public function dishes_1Edit($id) {
        $task = Task::findOrFail($id);
        $data = [];
        $data['users'] = User::where('admin_show',1)->get();
        $data['events'] = Event::where('admin_show',1)->get();
        $data['categories'] = Task::where('admin_show',1)->where('category','!=',"")->groupBy('category')->get();
        
       
        $data['partialView'] = 'Task.dishes_1';
        $data['task'] = $task;
        return view('Task.base', $data);
    }
    public function dishes_1Update(Request $request, $id) {
        $data = $request->input();
        if($data['user_id'] == ""){
            $data['confirmed'] = Null;
            $data['assigned_by_id'] = Null;
        }

        if($data['new_type'] != ""){
            $data['category'] = $data['new_type'];
            unset($data['new_type']);

        }
        $task = Task::findOrFail($id);
        $event = Event::find($data['event_id']);
        // if the task is assigned to a user send notification to the user
        if($data['user_id'] != $task->user_id && $data['user_id'] != ""){
            $user = User::find($data['user_id']);
                $subject="New Task Assigned in ".$event->name;
                $msg="
                ".Auth::user()->name." has assigned you the task ".$task->name." in ".$event->name.". Please accept or reject this task by signing in and going to your <a href='".route('event_user.invitationsindex')."'>My Events page  </a> and clicking on the Accept/Reject buttons.";
                $title="New Task Assigned";
                Helpers::sendEmail($subject,$msg,$user,$title);


        }
        // if adding new task send email to all event participants
        if($task->admin_show==0){

            
            
            $event_users = EventUser::where('event_id',$event->id)->where('status','!=','Rejected')->get();
            
            foreach($event_users as $event_user){
                $user = User::find($event_user->user_id);
                $subject="A New Task Has Been Added to ".$event->name."";
                $msg="<p>Dear All, </p>

<p>".Auth::user()->name." has added ".$task->name." to the list of tasks in ".$event->name.". Feel free to pick up this task if you are interested</p>

<p>Thanks,</p>
<p>".Auth::user()->name."</p>

<p>To access the event, <a href='".route('event.event_details',['event_id' => $event->id])."'>Click Here</a> </p>";
                $title="New Task Added to Event";
                if($user){
                    Helpers::sendEmail($subject,$msg,$user,$title);
                }

            }
        }
        
        
        $task->admin_show=1;
        $task->update($data);//if you use array inputs, you should use $request->except([array inputs])
        return response()->json(['status'=>'success','msg'=>'Task successfully saved','url' => route('task.dishesindex')]);
        }
    public function init() {
        $task = new Task();
        $task->save();
        return redirect(route('task.dishes_1Edit', ['id' => $task->id]));
    }
    public function pickup($task_id,$user_id) {
        $task = Task::find($task_id);
        $task->user_id = $user_id;
        $task->assigned_by_id = $user_id;
        $task->save();
        $participant = User::find($user_id);
        $event = Event::find($task->event_id);

        $user = User::where("user_type","User")->first();
                $subject=$participant->name." Wishes to Handle ".$task->name." in ".$event->name;
                $msg="
".$participant->name." has picked up the task ".$task->name." in the ".$event->name." event. Please visit <a href = '".route('event.event_details',['event_id' => $event->id])."'>this page </a> to approve or disapprove. 
";
                $title="New Task Request";
                Helpers::sendEmail($subject,$msg,$user,$title);

        
    }
    public function comments($task_id) {
        $task = Task::find($task_id);
        $comments = Comment::where('event_id',$task->event_id)->where('task_id',$task_id)->orderBy('created_at','DESC')->get();
        $comments_count = Comment::where('event_id',$task->event_id)->where('task_id',$task_id)->count();

        $data['partialView'] = 'Task.comments';
        $data['task'] = $task;
        $data['comments'] = $comments;
        $data['comments_count'] = $comments_count;
        return view('Task.base', $data);
        
        
        
    }
    public function delete($id) {  
    $task = Task::find($id);
    $event = Event::find($task->event_id);
    $user = User::find($task->user_id);      
        if($task->user_id != ""){
            $subject=Auth::user()->name." Has Removed ".$task->name." From ".$event->name;
                $msg=Auth::user()->name." Has removed the task ".$task->name." assigned to you. Please contact event organizer for further information 
";
                $title="Task Removed from Event";
                Helpers::sendEmail($subject,$msg,$user,$title);

        }
        Task::destroy($id) or abort(404);    
    }
//functions


}
