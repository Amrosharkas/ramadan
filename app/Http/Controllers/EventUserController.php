<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\EventUser;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Schema;
use Auth;
use App\Task;
use App\Http\Helpers\Helpers;
use App\User;
use App\Event;
//uses
class EventUserController extends Controller {


    public function invitationsIndex() {
        $data = [];
        //@@some-data@@
        $data['partialView'] = 'EventUser.invitations';
        $data['event_users'] = EventUser::orderBy('created_at', 'asc')->where('user_id',Auth::user()->id)->where('status','Pending')->get();
        $data['user_events'] = EventUser::orderBy('created_at', 'asc')->where('user_id',Auth::user()->id)->whereIn('status',array('Accepted','Rejected'))->get();
        $data['my_tasks'] = Task::orderBy('created_at', 'asc')
        ->where('user_id',Auth::user()->id)
        
        ->get();

        return view('EventUser.base', $data);
    }

    public function respondToEventInvitation($id,$reply){
    	$invitation = EventUser::find($id);
        $participant = User::find($invitation->user_id);
        $event = Event::find($invitation->event_id);
        $user = User::where('user_type','User')->first();
        if($reply == "Accepted" ){
            // Accepting event
            if($invitation->status =="Pending"){
                $subject = "
".$participant->name." Accepted Your Invitation to  ".$event->name."
";
                $msg = "
<p>Congratulations, ".$participant->name." has accepted your invitation to ".$event->name."</p>

<p>To access the event, <a href='".route('event.event_details',['event_id' => $event->id])."'>Click Here</a> </p>
            ";
                $title="Invitation Accepted";    
            }
            // Coming back to an event
            else{
                $subject = "
".$participant->name." Has Returned to ".$event->name."
";
                $msg = "
<p>Congratulations, ".$participant->name." has returned to  ".$event->name."</p>

<p>To access the event, <a href='".route('event.event_details',['event_id' => $event->id])."'>Click Here</a> </p>
            ";
                $title="Participant Returned to Event";    
            }

            }
            
            
            
           
            
        
        else{

            if($invitation->status =="Pending"){
                $subject = "
    ".$participant->name." Rejected Your Invitation to ".$event->name."
    ";
                $msg = "
    <p>Unfortunately, ".$participant->name." has declined your invitation to ".$event->name."</p>

    
                ";
                $title="Invitation Rejected";
            }
            else{
                $subject = "
    ".$participant->name." Has Left  ".$event->name."
    ";
                $msg = "
    <p>Unfortunately, ".$participant->name." has left  ".$event->name."</p>

    <p>Try to contact him to reconsider </p>
                ";
                $title="Participant Left Event";

            }
            
        }
    	$invitation->status = $reply;
    	$invitation->save();
        
        Helpers::sendEmail($subject,$msg,$user,$title);
        if($reply == "Accepted"){
            return response()->json(['page'=>route('event.event_details',['event_id' => $invitation->event_id ]) ]);
        }

    }

    public function respondToTaskInvitation($id,$reply){
    	$invitation = Task::find($id);
    	$invitation->confirmed = $reply;
        $event = Event::find($invitation->event_id);
        $task = $invitation;
        $participant = User::find($task->user_id);
    	if($reply ==0){
            $text = "Rejected";
            $text2 = "Unfortunately";
            $text3 = "Declined to Approve";
            $text4 ="Not Approved";
    		$invitation->user_id = Null;
            $invitation->confirmed = Null;    	
    	}else{
            $text = "Accepted";
            $text2 = "Congratulations";
            $text3 = "Approved";
            $text4 ="Approved";
        }
    	$invitation->save();
        // Organizer is reponding to a task 
        if(Auth::user()->user_type == "User"){
            $user=$participant;
            $subject = "
            ".Auth::user()->name." Has ".$text3." Assigning You ".$task->name." in ".$event->name."
     



    ";
                $msg = "".Auth::user()->name." has ".$text4." you to handle ".$task->name." in ".$event->name."
                ";
                $title="Request ".$text4;
                Helpers::sendEmail($subject,$msg,$user,$title);

        }
        // Participant is responding to am assigned task
        else{
            $user = User::where('user_type','User')->first();
            $subject = "
     

".$participant->name." Has ".$text." ".$task->name." in ".$event->name."

    ";
                $msg = "
    ".$text2.", ".$participant->name." has ".$text." the responsibility for ".$task->name." in ".$event->name."
                ";
                $title="Task ".$text;
                Helpers::sendEmail($subject,$msg,$user,$title);


        }
    }
//functions


}
