<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Group;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Schema;
use App\User;
//uses
class GroupController extends Controller {


    public function groupsIndex() {
        $data = [];
        //@@some-data@@
        $data['partialView'] = 'Group.groups';
        $data['groups'] = Group::orderBy('created_at', 'asc')->where('admin_show',1)->get();
        return view('Group.base', $data);
    }
    public function groups_1Edit($id) {
        $group = Group::findOrFail($id);
        $users = User::where('admin_show',1)->where('user_type','user')->get();
        $data = [];
    $data['partialView'] = 'Group.groups_1';
        $data['group'] = $group;
        $data['users'] = $users;
        
        return view('Group.base', $data);
    }
    public function groups_1Update(Request $request, $id) {
        $data = $request->input();
        if(isset($data['users'])){
            $data['users'] = implode (",", $data['users']);
        }else{
            return response()->json([
                'status'=>'error',
                'msg'=>'You must select at least one user',
                'url' => "none"
                ]);
        }
        $group = Group::findOrFail($id);
        
$group->admin_show=1;
$group->update($data);//if you use array inputs, you should use $request->except([array inputs])
return response()->json(['status'=>'success','msg'=>'Group successfully saved','url' => route('group.groupsindex')]);
    }
    public function init() {
        $group = new Group();
        $group->save();
        return redirect(route('group.groups_1Edit', ['id' => $group->id]));
    }
public function delete($id) {        Group::destroy($id) or abort(404);    
}
//functions


}
