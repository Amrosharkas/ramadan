<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Schema;
use Auth;
use App\Task;
//uses
class UserController extends Controller {

    public function index(){
        $user = Auth::user();
        if(!$user){
            return redirect('/login');
        }
        if($user->user_type === "User"){
            return redirect(route('user.usersindex'));
        }else{
            return redirect(route('event_user.invitationsindex'));    
        }
    }
    public function usersIndex() {
        $data = [];
        //@@some-data@@
        $data['partialView'] = 'User.users';
        $data['users'] = User::orderBy('created_at', 'asc')->where('admin_show',1)->get();
        return view('User.base', $data);
    }
    public function users_1Edit($id) {
        $user = User::findOrFail($id);
        $data = [];
    $data['partialView'] = 'User.users_1';
        $data['user'] = $user;
        return view('User.base', $data);
    }
    public function users_1Update(Request $request, $id) {
        $user = User::findOrFail($id);
        $password = bcrypt($request->password);

$user->update( $request->except(['password']));
        $user->admin_show=1;
$user->update([ 'password' => $password]);//if you use array inputs, you should use $request->except([array inputs])

        return response()->json(['status'=>'success','msg'=>'User successfully saved','url' => route('user.usersindex')]);
    }
    public function init() {
        $user = new User();
        $user->save();
        return redirect(route('user.users_1Edit', ['id' => $user->id]));
    }
public function delete($id) {        
    User::destroy($id) or abort(404);  
    Task::where('user_id',$id)->update(["user_id"=>Null]);
    Task::where('assigned_by_id',$id)->update(["assigned_by_id" => Null]);  
}
//functions


}
