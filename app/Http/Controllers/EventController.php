<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Event;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Schema;
use App\User;
use App\EventUser;
use App\Group;
use App\GroupUser;
use Auth;
use Mail;
use App\File;
use App\Comment;
use App\Http\Helpers\Helpers;
use App\Task;
//uses
class EventController extends Controller {


    public function eventsIndex() {
        $data = [];
        //@@some-data@@
        $data['partialView'] = 'Event.events';
        $data['events'] = Event::orderBy('created_at', 'asc')->where('admin_show',1)->get();
        return view('Event.base', $data);
    }
    public function events_1Edit($id) {
        $event = Event::findOrFail($id);
        $users = User::where('admin_show',1)->where('user_type','user')->get();
        $groups = Group::where('admin_show',1)->get();
        $data = [];
    $data['partialView'] = 'Event.events_1';
        $data['event'] = $event;
        $data['users'] = $users;
        $data['groups'] = $groups;
        return view('Event.base', $data);
    }
    public function events_1Update(Request $request, $id) {
        $data = $request->input();
        $event = Event::findOrFail($id);
        
$event->admin_show=1;
$event->update($request->all());//if you use array inputs, you should use $request->except([array inputs])
// invite users
if(isset($data['users'])){
    foreach($data['users'] as $user){
        $check_already_invited = EventUser::where('event_id',$id)->where('user_id',$user)->count();
        if($check_already_invited == 0){
            $new_invitation = new EventUser();
            $new_invitation->user_id = $user;
            $new_invitation->event_id = $id;
            $new_invitation->status = "Pending";
            $new_invitation->participant = 1;
            $new_invitation->owner = 0;
            $new_invitation->orgnizer = 0;
            $new_invitation->invited_by = Auth::user()->id;
            $new_invitation->save();
            $event = Event::find($id);
            $user= User::find($user);
                $subject = Auth::user()->name." Invites you to ".$event->name;
                $msg = "<p>Dear ".$user->name.",</p>

".Auth::user()->name." <p>has invited to attend ".$event->name." hosted at ".$event->location." at ".$event->start_time." and would very much like for you to accept.</p>

<p>To accept please <a href='http://events.arabiclocalizer.info'>click here</a> and login with the following credentials:</p>
<p>Username: ".$user->email."</p>
<p>Password: 123456 </p>

<p>Please feel free to pick up a task from the list of tasks available in the event and wait for the event organizer’s approval. </p>

<p>Many Thanks,</p>
".Auth::user()->name."";
$title = "New Event Invitation";
                Helpers::sendEmail($subject,$msg,$user,$title);
        }

    }
}
// invite groups
if(isset($data['groups'])){
    foreach($data['groups'] as $group_id){
        $group = Group::find($group_id);
        //dd($group);
        $user_emails = explode(",", $group->users);
        foreach($user_emails as $email){
            $user = User::where('email',$email)->first();
            
            $check_already_invited = EventUser::where('event_id',$id)->where('user_id',$user->id)->count();
            if($check_already_invited == 0){
                $new_invitation = new EventUser();
                $new_invitation->user_id = $user->id;
                $new_invitation->event_id = $id;
                $new_invitation->status = "Pending";
                $new_invitation->participant = 1;
                $new_invitation->owner = 0;
                $new_invitation->orgnizer = 0;
                $new_invitation->invited_by = Auth::user()->id;
                $new_invitation->save();

                $event = Event::find($id);
                $subject = Auth::user()->name." Invites you to ".$event->name;
                $msg = "<p>Dear ".$user->name.",</p>

".Auth::user()->name." <p>has invited to attend ".$event->name." hosted at ".$event->location." at ".$event->start_time." and would very much like for you to accept.</p>

<p>To accept please <a href='http://events.arabiclocalizer.info'>click here</a> and login with the following credentials:</p>
<p>Username: ".$user->email."</p>
<p>Password: 123456 </p>

<p>Please feel free to pick up a task from the list of tasks available in the event and wait for the event organizer’s approval. </p>

<p>Many Thanks,</p>
".Auth::user()->name."";
$title = "New Event Invitation";
                Helpers::sendEmail($subject,$msg,$user,$title);
            }

        }
        

    }
}
// make the admin a participant
$check_already_invited = EventUser::where('event_id',$id)->where('user_id',Auth::user()->id)->count();
            if($check_already_invited == 0){
                $new_invitation = new EventUser();
                $new_invitation->user_id = Auth::user()->id;
                $new_invitation->event_id = $id;
                $new_invitation->status = "Accepted";
                $new_invitation->participant = 1;
                $new_invitation->owner = 1;
                $new_invitation->orgnizer = 1;
                $new_invitation->save();
            }
return response()->json(['status'=>'success','msg'=>'Event successfully saved','url' => route('event.eventsindex')]);
    }
    public function init() {
        $event = new Event();
        $event->save();
        return redirect(route('event.events_1Edit', ['id' => $event->id]));
    }

    public function event_details($id) {
        $event = Event::findOrFail($id);
        $users = User::where('admin_show',1)->where('user_type','user')->get();
        $groups = Group::where('admin_show',1)->get();
        $files = File::get();
        $comments = Comment::where('event_id',$event->id)->where('task_id',0)->orderBy('created_at','DESC')->get();
        $comments_count = Comment::where('event_id',$event->id)->where('task_id',0)->count();
        $data = [];
    $data['partialView'] = 'Event.event_details';
        $data['event'] = $event;
        $data['users'] = $users;
        $data['files'] = $files;
        $data['comments'] = $comments;
        $data['comments_count'] = $comments_count;
        $data['groups'] = $groups;
        return view('Event.base', $data);
    }
public function delete($id) {        Event::destroy($id) or abort(404);    
}

public function do_comment(Request $request){
    $data = $request->input();
    $comment = new Comment();
    $comment->comment = $data['comment'];
    $comment->event_id = $data['event_id'];
    $comment->task_id = $data['task_id'];
    if(isset($data['memory_id'])){
        $comment->memory_id = $data['memory_id'];    
    }
    $comment->user_id = Auth::user()->id;
    $comment->save();
    $time = date("l h:i a",strtotime($comment->created_at));
    $total_comments = Comment::where('memory_id',$data['memory_id'])->count();
    

    $event = Event::find($data['event_id']);
    
    if($data['task_id'] != 0){
        $task = Task::find($data['task_id']);
        $subject = Auth::user()->name." Added a New Comment to ".$task->name." in ".$event->name;
                $msg = Auth::user()->name." <p>has commented on your ".$task->name." task in ".$event->name." with the following:</p>
<p>".$comment->comment."</p>

To access the event, <a href='".route('event.event_details',['event_id' => $data['event_id']])."' >Click Here</a> ";
$title = "New Comment on a Task Assigned to You";
        // send email to the user assigned to this task
        if($task->user_id && $task->user_id != Auth::user()->id){
            $user = User::find($task->user_id);
            
                Helpers::sendEmail($subject,$msg,$user,$title);
        }
        // send email to all the participants in this thread
        $comments = Comment::where('task_id',$data['task_id'])->groupBy('user_id')->get();
        foreach($comments as $commentt){
            $user = User::find($commentt->user_id);
            if($user->id != Auth::user()->id){
                Helpers::sendEmail($subject,$msg,$user,$title);

            }

        }    
    }

    if(isset($data['memory_id'])){
$memory = File::find($data['memory_id']);
        $subject = Auth::user()->name." Has commented on a memory ".$memory->file;
                $msg = Auth::user()->name." <p>has commented on a memory ".$memory->file." </p>

To access the event, <a href='".route('event.event_details',['event_id' => '1'])."' >Click Here</a> ";
$title = "New Comment Added";
        // send email to the user assigned to this task
        if($memory->user_id && $memory->user_id != Auth::user()->id){
            $user = User::find($memory->user_id);
            
                Helpers::sendEmail($subject,$msg,$user,$title);
        }
        // send email to all the participants in this thread
        $comments = Comment::where('memory_id',$data['memory_id'])->groupBy('user_id')->get();
        foreach($comments as $commentt){
            $user = User::find($commentt->user_id);
            if($user->id != Auth::user()->id){
                Helpers::sendEmail($subject,$msg,$user,$title);

            }

        }       
    }


    

return response()->json(['page'=>'none' ,'name' => Auth::user()->name , 'time' => $time,'comment'=>$comment->comment,'total_comments'=>$total_comments]);

}

public function show_memory($id){
    $memory = File::find($id);
    $comments = Comment::where('memory_id',$memory->id)->orderBy('created_at','DESC')->get();
    $comments_count = Comment::where('memory_id',$memory->id)->count();
    $data['partialView'] = 'Event.show_memory';
    $data['memory'] = $memory;
    $data['comments'] = $comments;
    $data['comments_count'] = $comments_count;
    return view('Event.base', $data);

}
//functions


}
