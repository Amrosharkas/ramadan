<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class events_1Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|' ,
            'location'=>'required|' ,
            'latitude'=>'' ,
            'longitude'=>'' ,
            'cost'=>'' ,
            'reminders'=>'' ,
            'event_image'=>'' ,
            'description'=>'' ,
            'end_time'=>'required|' ,
            'start_time'=>'required|' ,

        ];
    }
}
