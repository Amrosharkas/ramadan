<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class dishesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|' ,
            'user_id'=>'' ,
            'assigned_by_id'=>'' ,
            'description'=>'' ,
            'confirmed'=>'' ,
            'deadline'=>'' ,
            'type'=>'' ,
            'cost'=>'' ,
            'event_id'=>'required|' ,
            'task_image'=>'' ,

        ];
    }
}
