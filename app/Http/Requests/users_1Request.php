<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class users_1Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|' ,
            'user_type'=>'' ,
            'email'=>'email|required|' ,
            'password'=>'required|confirmed' ,
            'password_confirmation'=>'required|' ,

        ];
    }
}
