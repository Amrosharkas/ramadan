<?php

namespace App\Http\Helpers;

use App\User;
use Auth;
use DB;
use Mail;


 class Helpers
{
	public static function sendEmail($subject,$msg,$user,$title){
		Mail::send('emails.template', ['msg' => $msg  ,'title' =>$title], function ($m) use ($user,$subject) {
	        $m->from('notifications@arabiclocalizer.info', 'Dish Party | Arabic Localizer');

	        $m->to($user->email, $user->name)->subject($subject);

    
         });

	}

}