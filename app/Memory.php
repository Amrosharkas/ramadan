<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Memory extends Model
{
    protected $table='memories';
    protected $fillable=[ 'album_id', 'user_id', 'event_id', 'file_id',];
    
 public function album(){
        return $this->belongsTo('App\Album' ,  'album_id');
 }
 public function user(){
        return $this->belongsTo('App\User' ,  'user_id');
 }
 public function event(){
        return $this->belongsTo('App\Event' ,  'event_id');
 }
 public function file(){
        return $this->belongsTo('App\File' ,  'file_id');
 }
}
