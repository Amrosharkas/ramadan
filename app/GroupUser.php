<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  GroupUser extends Model
{
    protected $table='group_users';
    protected $fillable=[ 'group_id', 'user_id',];
    
 public function group(){
        return $this->hasOne('App\Group' , group);
 }
 public function user(){
        return $this->hasOne('App\User' , user);
 }
}
