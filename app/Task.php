<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Task extends Model
{
    protected $table='tasks';
    protected $fillable=[ 'name', 'user_id', 'assigned_by_id', 'description', 'confirmed', 'deadline', 'type', 'cost', 'event_id', 'task_image', 'category'];
    
 public function user(){
        return $this->belongsTo('App\User' ,  'user_id');
 }
 public function assigned_by(){
        return $this->belongsTo('App\User' ,  'assigned_by_id');
 }
 public function event(){
        return $this->belongsTo('App\Event' ,  'event_id');
 }

 public function commentsCount(){
        return $this->hasMany('App\Comment');
 }
}
