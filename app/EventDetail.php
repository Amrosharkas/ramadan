<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  EventDetail extends Model
{
    protected $table='event_details';
    protected $fillable=[ 'event_id', 'date_from', 'date_to',];
    
 public function event(){
        return $this->belongsTo('App\Event' ,  'event_id');
 }
}
