<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-globe font-color"></i> Event
        </div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a class="btn color pjax-link"  href="{{route('event.init')}}">
                            Add New <i class="fa fa-plus"></i>
                        </a>
                       
                    </div>
                </div>
{{--@reorder-button@--}}

            </div>
        </div>

    </div>
    <form id="search-form">
        {{csrf_field()}}
        
        <div class="form-group"></div>
    </form>

    <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">

        <thead>
        <tr class="tr-head">
            <th valign="middle" class="no-sorting table-checkbox-col">
                <input type="checkbox" class="group-checkable">
            </th>
            <th valign="middle">
                        Event Name
                    </th>
<th valign="middle">
                        Location
                    </th>
<th valign="middle">
                        End Time
                    </th>
<th valign="middle">
                        Start Time
                    </th>
<th valign="middle">
                        Actions
                    </th>

        </tr>
        </thead>
        <tbody>
        @foreach($events as $index => $event)
        <tr class="odd gradeX" id="data-row-{{$event->id}}">
           <td valign="middle">
               <input type="hidden" value="{{$event->id}}" class="reorder-vals"/>
               <input type="checkbox" name="items[]" class="table-checkbox" value="{{$event->id}}"/>
           </td>
            <td valign="middle">
            <a href="{{route('event.event_details' , ['event_id' => $event->id])}}">
                        {{$event->name}}
                        </a>
                    </td>
<td valign="middle">
                        {{$event->location}}
                    </td>
<td valign="middle">
                        {{$event->end_time}}
                    </td>
<td valign="middle">
                        {{$event->start_time}}
                    </td>
<td valign="middle">
                        <a href="{{route('event.events_1Edit',['id'=>$event->id])}}" class="btn btn-edit  pjax-link" ><i class="fa fa-edit"></i> Edit</a>
                        <a href="{{route('event.delete',['id'=>$event->id ])}}" data-method="delete" class="btn btn-delete remove-stuff" ><i class="fa fa-remove"></i> Delete</a>
                    </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>