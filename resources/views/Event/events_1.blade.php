<div class="portlet light bordered">
<div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-gift font-color"></i>Event
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form id="main-form"  class="ajax_form j-forms" method="post" action="{{route('event.events_1Edit' , ['id'=>$event->id])}}">
            {{csrf_field()}}
            <div class="content">
                  <div class="unit form-group ">
                    <label class="label">Event Name</label>

                    <input value="{{$event->name}}" class="form-control" type="text" name="name" id="name" data-name="Event Name"  data-validation=",required" />
</div>
<div class="unit form-group ">
                    <label class="label">Location</label>

                    <input value="{{$event->location}}" class="form-control" type="text" name="location" id="location" data-name="Location"  data-validation=",required" />
</div>

<div class="unit form-group ">
                    <label class="label">Description</label>
                    <div class="input">
					<label class="icon-right">
                        <i class="fa fa-edit"></i>
                    </label>
                    <textarea  class="form-control"  name="description" id="description" data-name="Description"  data-validation="" />
                    {{$event->description}}
                    </textarea>
                    </div>
</div>

<div class="unit form-group ">
                    <label class="label">Start Time</label>
                <div class="input">
                    <label class="icon-left" >
                            <i class="fa fa-calendar "></i>
                        </label>
                    <input value="{{$event->start_time}}" class="form-control timepicker val" type="text" name="start_time" id="start_time" data-name="This Field"  data-validation=",required" />
                    <span class="error_message error-view"></span>
                </div>                                              
</div>
<div class="unit form-group ">
                    <label class="label">End Time</label>
                <div class="input">
                    <label class="icon-left" >
                            <i class="fa fa-calendar "></i>
                        </label>
                    <input value="{{$event->end_time}}" class="form-control timepicker val" type="text" name="end_time" id="end_time" data-name="This Field"  data-validation=",required" />
                    <span class="error_message error-view"></span>
                </div>                                              
</div>
<div class="unit form-group ">
                    <label class="label">Invite Users</label>

                    <select  name="users[]" data-validation="required" data-name="users" class="form-control select2-multiple select2" multiple>
                                            <optgroup label="users">
                                            	@foreach($users as $user)
                                                <option value="{{$user->id}}" >{{$user->name}}</option>
                                                @endforeach
                                                
                                            </optgroup>
                                        </select>
</div>

<div class="unit form-group ">
                    <label class="label">Invite Groups</label>

                    <select  name="groups[]" data-validation="required" data-name="groups" class="form-control select2-multiple select2" multiple>
                                            <optgroup label="Groups">
                                            	@foreach($groups as $group)
                                                <option value="{{$group->id}}" >{{$group->name}}</option>
                                                @endforeach
                                                
                                            </optgroup>
                                        </select>
</div>

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                <!-- save as-->
                <input name="publish" type="submit" value="Save" class="btn btn-lg primary-btn"
                 id="publish"/>
                </div>
            </div>
        </form>
        <!-- END FORM-->
</div>

</div>

@include('vendor.lrgt.ajax_script', ['form' => '#main-form',
'request'=>'App/Http/Requests/events_1Request','on_start'=>false])