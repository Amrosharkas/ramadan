<style>
.comment_div{
	margin-left:10px;
	margin-bottom:30px;
}
.comment_div h5{
	color:#0088cc !important; 
	border-bottom:1px solid #d8d8d8; 
	padding-bottom:8px; 
	font-weight:bold;
}
.comment_div h5 span{
	color:#999; 
	font-weight:normal;
}
.comment_div p{
	margin:10px 0 !important;
}
.add_memory{
	position:fixed;
	top:50px;
	right:0;
	z-index:10000000000000000000000000000;
	opacity:0.8;
}
.add_memory:hover{
	opacity:1;
}
</style>
<a class="btn btn-success add_memory popup" href="{{route('files.add')}}"> + Add memory </a>
<div class="portlet light bordered">

    <div class="portlet-body form">
        <div class="row">
				<div class="col-md-12 news-page blog-page">
					<div class="row">
					  <div class="col-md-6 blog-tag-data">
							<img src="/images/cover.jpg" class="img-responsive" alt="">
                            <div class="row">
                            &nbsp;
                            </div>
							<div class="row">
								<div class="col-md-6">
									<ul class="list-inline blog-tags">
										<li>
											<i class="fa fa-map-marker"></i>
											
											{{$event->location}} 
									  </li>
									</ul>
								</div>
								<div class="col-md-6 blog-tag-data-inner">
									<ul class="list-inline">
										<li>
											<i class="fa fa-calendar"></i>
											
											{{ Carbon\Carbon::parse($event->start_time)->format('l M d, Y') }} 
									  </li>
                                        
                                      <i class="fa fa-clock-o"></i>
											
											{{ Carbon\Carbon::parse($event->start_time)->format('h:i a') }} - 
                                            {{ Carbon\Carbon::parse($event->end_time)->format('h:i a') }} 
										</li>
										
								  </ul>
								</div>
							</div>
							<div class="news-item-page">
								<p>
									 {{$event->description}}
							  </p>
								
								
						</div>
							<hr>
							<!--end media--><!--end media-->
							<hr>
					  </div>
					  <div class="col-md-6">
							
						  <div class="space20">
					      </div>
						  <div class="space20">
						    </div>
							
							<div class="tabbable-line">
								<ul class="nav nav-tabs">
									<li class="active">
										<a data-toggle="tab" href="#tab_1_1">
									  Participants </a>
									</li>
									<li class="hide">
										<a data-toggle="tab" href="#tab_1_2">
									  Invited </a>
									</li>
								</ul>
								<div class="tab-content">
									<div id="tab_1_1" class="tab-pane active" style="height:580px; overflow:auto;">
										<table class="table table-striped table-bordered table-advance table-hover">
														
														<tbody>
                                                        @foreach($event->AcceptedUsers as $user)
														<tr>
															<td>
																<a href="javascript:;">
																<i class="fa fa-user" aria-hidden="true"></i> &nbsp;{{$user->name}} </a>
															</td>
															<td width="20" align="center" bgcolor="#669900" ><div class="tooltips"  data-placement="top" data-original-title="Attending" style="width:10px; height:20px;">&nbsp;</div></td>
						                  </tr>
                                                          @endforeach
                                                          @foreach($event->PendingUsers as $user)
														<tr>
														  <td>
																<a href="javascript:;">
																<i class="fa fa-user" aria-hidden="true"></i> &nbsp; {{$user->name}} </a>
															</td>
															<td width="20" align="center" bgcolor="#0088cc" >
                                                            <div class="tooltips"  data-placement="top" data-original-title="Pending" style="width:10px; height:20px;">&nbsp;</div>
                                                            
                                                            </td>
					                      </tr>
                                                          @endforeach
														
														</tbody>
													                                                                                                                      
														</table>
                                                 
									</div>
									<div id="tab_1_2" class="tab-pane">
                                    <table class="table table-striped table-bordered table-advance table-hover">
														
														<tbody>
                                                        @foreach($event->PendingUsers as $user)
														<tr>
															<td>
																<a href="javascript:;">
																{{$user->name}} </a>
															</td>
									  </tr>
                                                          @endforeach
														
														</tbody>
									  </table>
										
								  </div>
								</div>
							</div>
							<div class="space20">
							</div>
							<div class="space20">
						    </div>
					  </div>
					</div>
				</div>
			</div>
</div>

</div>
@if(count($files)!= 0)
<h3> Memories </h3>
<div id="gallery">

<div id="gallery-content">
<div id="gallery-content-center">
@foreach($files as $file)
        	@if($file->processed == 0)
            <a href="{{route('event.show_memory',['memory_id' => $file->id])}}" class="gallery-item" >
            	<img src="/uploads/{{$file->hash}}/{{$file->file}}" style="max-width:250px;" />
            </a>
            @endif
            @if($file->processed == 1)
            <a href="{{route('event.show_memory',['memory_id' => $file->id])}}" class="gallery-item" >
            	<img src="https://img.youtube.com/vi/{{$file->youtube_id}}/0.jpg" style="max-width:250px;" />
            </a>
            @endif
            @if($file->processed == 2)
            <a href="{{route('event.show_memory',['memory_id' => $file->id])}}" data-id="{{$file->id}}"  class="gallery-item refresh_video" title="Video is processing, refresh the page later"  >
            	<img src="/images/loadin.gif" style="max-width:150px;" />
            </a>
            @endif
        @endforeach




</div>
</div>
</div>
</div>
@endif

<div class="portlet light bordered">
<div class="portlet-title">
        <div class="caption font-color">
          <i class="fa fa-check font-color"></i> Pick up a dish to prepare
      </div>

    <div class="portlet-body form">
        <table class="table table-striped table-bordered table-hover table-dt" >

        
        <tbody>
        <?php $old_cat = "";?>
        @foreach($event->PoolDishes as $index => $task)
        <?php $new_cat = $task->category ;?>
        @if($new_cat != $old_cat)
        <tr class="odd gradeX">
          <td colspan="4" valign="middle" bgcolor="#0088CC"><span style="color:#fff; font-weight:bold;">{{$task->category}}</span></td>
          </tr>
          
        <tr class="tr-head">
            <th valign="middle">Dish</th>
            <th valign="middle">Description</th>
            <th valign="middle">Category</th>
            <th valign="middle">
                        Actions
                    </th>
</tr>
        
          @endif
        <tr class="odd gradeX" id="data-row-{{$task->id}}">
           <td valign="middle">{{$task->name}} <a href="{{route('task.comments',['task_id' => $task->id])}}" class="popup" ><i class="fa fa-edit font-color"></i> Comments {{$task->commentsCount->count()}}</a></td>
           <td valign="middle">{{$task->description}}</td>
           <td valign="middle">{{$task->category}}</td>
          <td valign="middle">
            
            <a href="{{route('task.pickup',['task_id'=>$task->id , 'user_id' => Auth::user()->id ])}}" data-method="get" class="btn btn-success confirm_action" ><i class="fa fa-check"></i> Pickup</a>
            
            
          </td>
</tr>
<?php $old_cat = $task->category ;?>
        @endforeach
    </table>
</div>

</div>
</div>

<div class="portlet light bordered">
<div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-check font-color"></i> Picked up dishes
      </div>

    <div class="portlet-body form">
        <table class="table table-striped table-bordered table-hover table-dt" >

        <thead>
        <tr class="tr-head">
            <th valign="middle">Dish</th>
            <th valign="middle">Description</th>
            <th valign="middle">Category</th>
            <th valign="middle">Assigned to</th>
            <th valign="middle">Assigned By</th>
            <th valign="middle">Approved By</th>
          <th valign="middle">
                        Status
                    </th>
          </tr>
        </thead>
        <tbody>
        @foreach($event->AssignedDishes as $index => $task)
         
        <tr class="odd gradeX" id="data-row-{{$task->id}}">
           <td valign="middle">{{$task->name}} <a href="{{route('task.comments',['task_id' => $task->id])}}" class="popup" ><i class="fa fa-edit font-color"></i> Comments {{$task->commentsCount->count()}}</a></td>
           <td valign="middle">{{$task->description}}</td>
           <td valign="middle">{{$task->category}}</td>
           <td valign="middle">
           <?php if(isset($task->user->name)){?>
           {{$task->user->name}}
           <?php ;}?>
           </td>
           <td valign="middle">
           <?php if(isset($task->assigned_by->name)){?>
                        {{$task->assigned_by->name}}
                        <?php ;}?>
                    </td>
          <td valign="middle">
          @if($task->assigned_by_id == $task->user_id)
          Organizer
          @else
          <?php if(isset($task->user->name)){?>
          {{$task->user->name}}
          <?php ;}?>
          @endif
          </td>
          <td valign="middle">
          @if($task->confirmed == 1)
                        <button class="btn btn-success" style="background-color:#669900 !important; border-color:#669900 !important;" ><i class="fa fa-check-circle" aria-hidden="true"></i> Confirmed</button>
                        @else
                       <button class="btn btn-primary" ><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Pending</button>
                       
                        @endif
                        @if(Auth::user()->id == $task->user_id)
                       <a href="{{route('event_user.reply_task',['id'=>$task->id , 'reply' => 0 ])}}" data-method="get" class="btn btn-delete confirm_action" ><i class="fa fa-remove"></i> Release</a>
                        @endif
                    </td>
          </tr>
        @endforeach
    </table>
</div>

</div>
</div>


<h3> Comments (<span id="comments_count">{{$comments_count}}</span>)</h3>
<form id="event_comments" class="ajax_form j-forms" method="post"
 action="{{route('comment.do_comment')}}">
 <div class="unit form-group ">
                    
                    <div class="input">
					<label class="icon-right">
                        <i class="fa fa-comment"></i>
                    </label>
                    <textarea  class="form-control comment"  name="comment"  data-name="Comment"  data-validation="" placeholder="Add a comment..." /></textarea>
                    </div>
</div>
<input name="event_id" value="{{$event->id}}" type="hidden" />
<input name="task_id" value="0" type="hidden" />
<input type="reset" name="Reset" id="reset_button" value="Reset" style="display:none;" />
</form>
<div id="comment_container">

@foreach($comments as $comment)
<div class="comment_div" >
<h5 ><?php if(isset($comment->user->name)){?>{{$comment->user->name}}<?php ;}?> <span class="pull-right" ><?php echo date("l h:i a",strtotime($comment->created_at));?></span></h5>
<p>{{$comment->comment}}</p>
</div>
@endforeach
<div class="comment_div" style="display:none;">
<h5 >@@name@@ <span class="pull-right" >@@time@@</span></h5>
<p>@@comment@@</p>
</div>

</div>
<div class="comment_div clone_comment" style="display:none;">
<h5 >--name-- <span class="pull-right" >--time--</span></h5>
<p>--comment--</p>
</div>





        

    
