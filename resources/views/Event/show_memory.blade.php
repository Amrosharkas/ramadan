<style>
.comment_div{
	margin-left:10px;
	margin-bottom:30px;
}
.comment_div h5{
	color:#0088cc !important; 
	border-bottom:1px solid #d8d8d8; 
	padding-bottom:8px; 
	font-weight:bold;
}
.comment_div h5 span{
	color:#999; 
	font-weight:normal;
}
.comment_div p{
	margin:10px 0 !important;
}
.add_memory{
	position:fixed;
	top:50px;
	right:0;
	z-index:10000000000000000000000000000;
	opacity:0.8;
}
.add_memory:hover{
	opacity:1;
}
.page-header,.clearfix,.page-footer,.page-title{
	display:none !important;
}
.page-container{
	margin:0 !important;
	padding:0 !important;
}
.row {
    margin-left: 0 !important;
    margin-right: 0 !important;
}
.col-sm-12,.col-sm-12{
	padding:0 !important;
}
</style>
<div class="row">
<div class="col-sm-12" style="text-align:center; background:#000;">
    @if($memory->youtube_id != "")
    <iframe width="100%" height="506" 
    src="https://www.youtube.com/embed/{{$memory->youtube_id}}">
    </iframe>
    @else
    <img src="/uploads/{{$memory->hash}}/{{$memory->file}}" style=" max-width:100%;" />
    @endif
</div>
<div class="col-sm-12">
<div style="padding:15px;">
    <h3> Comments (<span id="comments_count">{{$comments_count}}</span>)</h3>
    <form id="event_comments" class="ajax_form j-forms" method="post"
     action="{{route('comment.do_comment')}}">
     <div class="unit form-group ">
                        
                        <div class="input">
                        <label class="icon-right">
                            <i class="fa fa-comment"></i>
                        </label>
                        <textarea  class="form-control comment"  name="comment"  data-name="Comment"  data-validation="" placeholder="Add a comment..." /></textarea>
                        </div>
    </div>
    <input name="event_id" value="0" type="hidden" />
    <input name="task_id" value="0" type="hidden" />
    <input name="memory_id" value="{{$memory->id}}" type="hidden" />
    <input type="reset" name="Reset" id="reset_button" value="Reset" style="display:none;" />
    </form>
    <div id="comment_container">
    
    @foreach($comments as $comment)
    <div class="comment_div" >
    <h5 ><?php if(isset($comment->user->name)){?>{{$comment->user->name}}<?php ;}?> <span class="pull-right" ><?php echo date("l h:i a",strtotime($comment->created_at));?></span></h5>
    <p>{{$comment->comment}}</p>
    </div>
    @endforeach
    <div class="comment_div" style="display:none;">
    <h5 >@@name@@ <span class="pull-right" >@@time@@</span></h5>
    <p>@@comment@@</p>
    </div>
    
    </div>
    <div class="comment_div clone_comment" style="display:none;">
    <h5 >--name-- <span class="pull-right" >--time--</span></h5>
    <p>--comment--</p>
    </div>
    </div>
    </div>
</div>


