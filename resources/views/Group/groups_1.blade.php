<div class="portlet light bordered">
<div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-gift font-color"></i>Group
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form id="main-form"  class="ajax_form j-forms" method="post" action="{{route('group.groups_1Edit' , ['id'=>$group->id])}}">
            {{csrf_field()}}
            <div class="content">
                  <div class="unit form-group ">
                    <label class="label">Name</label>

                    <input value="{{$group->name}}" class="form-control" type="text" name="name" id="name" data-name="Name"  data-validation=",required" />
</div>
<div class="unit form-group ">
                    <label class="label">Users</label>

                    <select  name="users[]" data-validation="required" data-name="users" class="form-control select2-multiple select2" multiple>
                                            <optgroup label="users">
                                            	@foreach($users as $user)
                                                <option value="{{$user->email}}" <?php  if (strpos($group->users, $user->email) !== false) {?>selected="selected"<?php ;}?>>{{$user->name}}</option>
                                                @endforeach
                                                
                                            </optgroup>
                                        </select>
</div>

<div class="unit form-group ">
                    <input value="{{Auth::user()->id}}" class="form-control" type="hidden" name="created_by" id="created_by" data-name="created_by"  data-validation="" />
</div>

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                <!-- save as-->
                <input name="publish" type="submit" value="Save" class="btn btn-lg primary-btn"
                 id="publish"/>
                </div>
            </div>
        </form>
        <!-- END FORM-->
</div>
</div>

@include('vendor.lrgt.ajax_script', ['form' => '#main-form',
'request'=>'App/Http/Requests/groups_1Request','on_start'=>false])