<?php
$i = 'files';
$j = '';
?>
<style>
.page-header.navbar.navbar-fixed-top,.page-footer .page-footer-inner,h3.page-title {
    display: none !important;
}
</style>
<style>
.comment_div{
	margin-left:10px;
	margin-bottom:30px;
}
.comment_div h5{
	color:#0088cc !important; 
	border-bottom:1px solid #d8d8d8; 
	padding-bottom:8px; 
	font-weight:bold;
}
.comment_div h5 span{
	color:#999; 
	font-weight:normal;
}
.comment_div p{
	margin:10px 0 !important;
}
</style>
@extends('admin.master')
@section('plugins_css')

    <link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/sweetAlert/sweetalert.css')}}"/>
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" href="{{asset('assets/j-folder/css/j-forms.css')}}">
    <link rel="stylesheet" href="{{asset('assets/global/plugins/dragula/dragula.css')}}"/>
@endsection
@section('plugins_js')

    {{--<script language="JavaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
    <script type="text/javascript" src="{{asset('assets/global/scripts/datatable.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/global/plugins/malsup.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/global/plugins/sweetAlert/sweetalert.min.js')}}"></script>
    <script  type="text/javascript" src="{{asset('assets/global/plugins/dragula/dragula.js')}}"></script>
@endsection

@section('page_js')

    <script type="text/javascript" src="{{asset('assets/admin/pages/scripts/task.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/pages/scripts/comments.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/pages/scripts/ajaxForms.js')}}"></script>

@endsection


@section('add_inits')

@stop


@section('page_title_small')

@stop

@section('content')
    
<?php 
// Uploader instantiation
$selector = "uploader";
$multiple = "true";
$allowed_extentions =array();
$maximum_file_size = 500000000000000000000;
$youtube_videos = "true";
$encrypt_files = "true";
 
?>
<div id="{{$selector}}"></div>
@include('vendor.fine-uploader.uploader')
@stop