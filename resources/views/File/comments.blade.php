<style>
.page-header.navbar.navbar-fixed-top,.page-footer .page-footer-inner,h3.page-title {
    display: none !important;
}
</style>
<style>
.comment_div{
	margin-left:10px;
	margin-bottom:30px;
}
.comment_div h5{
	color:#0088cc !important; 
	border-bottom:1px solid #d8d8d8; 
	padding-bottom:8px; 
	font-weight:bold;
}
.comment_div h5 span{
	color:#999; 
	font-weight:normal;
}
.comment_div p{
	margin:10px 0 !important;
}
</style>
<h3> {{$task->name}} (<span id="comments_count">{{$comments_count}}</span>)</h3>
<form id="event_comments" class="ajax_form j-forms" method="post"
 action="{{route('comment.do_comment')}}">
 <div class="unit form-group ">
                    
                    <div class="input">
					<label class="icon-right">
                        <i class="fa fa-comment"></i>
                    </label>
                    <textarea  class="form-control comment"  name="comment"  data-name="Comment"  data-validation="" placeholder="Add a comment..." /></textarea>
                    </div>
</div>
<input name="event_id" value="{{$task->event_id}}" type="hidden" />
<input name="task_id" value="{{$task->id}}" type="hidden" />
<input type="reset" name="Reset" id="reset_button" value="Reset" style="display:none;" />
</form>
<div id="comment_container">

@foreach($comments as $comment)
<div class="comment_div" >
<h5 >{{$comment->user->name}} <span class="pull-right" ><?php echo date("l h:i a",strtotime($comment->created_at));?></span></h5>
<p>{{$comment->comment}}</p>
</div>
@endforeach
<div class="comment_div" style="display:none;">
<h5 >@@name@@ <span class="pull-right" >@@time@@</span></h5>
<p>@@comment@@</p>
</div>

</div>
<div class="comment_div clone_comment" style="display:none;">
<h5 >--name-- <span class="pull-right" >--time--</span></h5>
<p>--comment--</p>
</div>