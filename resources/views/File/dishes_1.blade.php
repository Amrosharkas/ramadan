<div class="portlet light bordered">
<div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-gift font-color"></i>Task
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form id="main-form"  class="ajax_form j-forms" method="post" action="{{route('task.dishes_1Edit' , ['id'=>$task->id])}}">
            {{csrf_field()}}
            <div class="content">
                  <div class="unit form-group ">
                    <label class="label">Name</label>

                    <input value="{{$task->name}}" class="form-control" type="text" name="name" id="name" data-name="Name"  data-validation=",required" />
</div>
<div class="unit form-group ">
                    <label class="label">Description</label>
                    <div class="input">
					<label class="icon-right">
                        <i class="fa fa-edit"></i>
                    </label>
                    <textarea  class="form-control"  name="description" id="description" data-name="Description"  data-validation="" />
                    {{$task->description}}
                    </textarea>
                    </div>
</div>
<div class="unit form-group " >
                    <label class="label">Category</label>
                    <label class="input select">
                        <select name="category" id="category" class="form-control " data-name="Assigned to" data-validation=" ">
                            <option value="" @if(! $task->user_id) selected @endif >Select Category</option>
                            @foreach($categories as $category)
                                <option @if($task->category == $category->category) selected @endif value="{{$category->category}}">{{$category->category}}</option>
                            @endforeach
                            <option value="New Value" >New</option>
                        </select>
                        <input style="display:none;" value="" class="form-control new_type" type="text" name="new_type" id="new_type" data-name="new_type"  data-validation="" />
                        <span class="error_message error-view"></span>
                        <i></i>
                    </label>
                </div>
<div class="unit form-group " >
                    <label class="label">Assigned to</label>
                    <label class="input select">
                        <select name="user_id" id="user_id" class="form-control " data-name="Assigned to" data-validation=" ">
                            <option value="" @if(! $task->user_id) selected @endif >Task Pool</option>
                            @foreach($users as $user)
                                <option @if($task->user_id && ($task->user_id == $user->id)) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                        <span class="error_message error-view"></span>
                        <i></i>
                    </label>
                </div>
<div class="unit form-group ">
                    <input value="{{Auth::user()->id}}" class="form-control" type="hidden" name="assigned_by_id" id="assigned_by_id" data-name="assigned_by_id"  data-validation="" />
</div>
<div class="unit form-group " >
          <label class="label">Event</label>
                    <label class="input select">
                        <select name="event_id" id="event_id" class="form-control " data-name="event_id" data-validation=",required ">
                            <option value="" @if(! $task->event_id) selected @endif disabled="">Select Event</option>
                            @foreach($events as $event)
                                <option @if($task->event_id && ($task->event_id == $event->id)) selected @endif value="{{$event->id}}">{{$event->name}}</option>
                            @endforeach
                        </select>
                        <span class="error_message error-view"></span>
                        <i></i>
                    </label>
                </div>

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                <!-- save as-->
                <input name="publish" type="submit" value="Save" class="btn btn-lg primary-btn"
                 id="publish"/>
                </div>
            </div>
        </form>
        <!-- END FORM-->
</div>
</div>

@include('vendor.lrgt.ajax_script', ['form' => '#main-form',
'request'=>'App/Http/Requests/dishes_1Request','on_start'=>false])