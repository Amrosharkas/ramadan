<div class="portlet light bordered">
<div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-gift font-color"></i>User
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form id="main-form"  class="ajax_form j-forms" method="post" action="{{route('user.users_1Edit' , ['id'=>$user->id])}}">
            {{csrf_field()}}
            <div class="content">
                  <div class="unit form-group ">
                    <label class="label">Name</label>

                    <input value="{{$user->name}}" class="form-control" type="text" name="name" id="name" data-name="Name"  data-validation=",required" />
</div>
<div class="unit form-group ">
                    <input value="user" class="form-control" type="hidden" name="user_type" id="user_type" data-name="user_type"  data-validation="" />
</div>
<div class="unit form-group ">
                    <label class="label">Email</label>

                    <input value="{{$user->email}}" class="form-control" type="text" name="email" id="email" data-name="Email"  data-validation=",email,required" />
</div>
<div class="unit form-group ">
                    <label class="label">Password</label>

                    <input class="form-control" type="password" name="password" id="password" data-name="Password"  data-validation=",required" />
</div>
<div class="unit form-group ">
                    <label class="label">Password</label>

                    <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" data-name="Password"  data-validation=",required" />
</div>

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                <!-- save as-->
                <input name="publish" type="submit" value="Save" class="btn btn-lg primary-btn"
                 id="publish"/>
                </div>
            </div>
        </form>
        <!-- END FORM-->
</div>
</div>

@include('vendor.lrgt.ajax_script', ['form' => '#main-form',
'request'=>'App/Http/Requests/users_1Request','on_start'=>false])