<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-globe font-color"></i> User
        </div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a class="btn color pjax-link"  href="{{route('user.init')}}">
                            Add New <i class="fa fa-plus"></i>
                        </a>
                       
                    </div>
                </div>
{{--@reorder-button@--}}

            </div>
        </div>

    </div>
    <form id="search-form">
        {{csrf_field()}}
        
        <div class="form-group"></div>
    </form>

    <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">

        <thead>
        <tr class="tr-head">
            <th valign="middle" class="no-sorting table-checkbox-col">
                <input type="checkbox" class="group-checkable">
            </th>
            <th valign="middle">
                        Name
                    </th>
<th valign="middle">
                        Email
                    </th>
<th valign="middle">
                        Actions
                    </th>

        </tr>
        </thead>
        <tbody>
        @foreach($users as $index => $user)
        <tr class="odd gradeX" id="data-row-{{$user->id}}">
           <td valign="middle">
               <input type="hidden" value="{{$user->id}}" class="reorder-vals"/>
               <input type="checkbox" name="items[]" class="table-checkbox" value="{{$user->id}}"/>
           </td>
            <td valign="middle">
                        {{$user->name}}
                    </td>
<td valign="middle">
                        {{$user->email}}
                    </td>
<td valign="middle">
                        <a href="{{route('user.users_1Edit',['id'=>$user->id])}}" class="btn btn-edit  pjax-link" ><i class="fa fa-edit"></i> Edit</a>
                        <a href="{{route('user.delete',['id'=>$user->id ])}}" data-method="delete" class="btn btn-delete remove-stuff" ><i class="fa fa-remove"></i> Delete</a>
                    </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>