<script>
        qq(window).attach("load", function() {
    "use strict";

    var errorHandler = function(id, fileName, reason) {
            return qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
        },
         validatingUploader;

    validatingUploader = new qq.FineUploader({
        element: document.getElementById("<?php echo $selector;?>"),
        multiple: <?php echo $multiple;?>,
        request: {
            endpoint: "{{route('files.endpoint')}}",
			params:{
				"_token" : "{{ csrf_token() }}"
				}
        },
		deleteFile: {
            enabled: true,
            endpoint: "/files/endpoint",
            forceConfirm: true,
			params:{
				"_token" : "{{ csrf_token() }}"
				}
        },
        debug: false,
        validation: {
            allowedExtensions: ["jpeg", "jpg", "MOV", "png","mpeg","MP4","AVI","WMV","FLV","3gp","WebM"],
            sizeLimit: <?php echo $maximum_file_size;?>,
            minSizeLimit: 2000
        },
        text: {
            uploadButton: "Click Or Drop"
        },
        display: {
            fileSizeOnSubmit: true
        },
		
        chunking: {
            enabled: true,
            concurrent: {
                enabled: false
            },
            success: {
                endpoint: "{{route('files.endpoint')}}?done"
            }
        },
        resume: {
            enabled: true
        },
        retry: {
            enableAuto: true
        },
		scaling: {
			sendOriginal: false,
			sizes: [
				{name: "original", maxSize: 900}
				
			]
		},
        callbacks: {
            onError: errorHandler,
			onComplete: function (id, filename,responseJSON,xhr) {
				var uuid = this.getUuid(id);
				$.ajax({
					url: "{{route('files.save_file')}}",
					method: "post",
					data:{
						uuid:uuid,
						name:filename,
						youtube_videos : <?php echo $youtube_videos;?>,
						encrypt_files: <?php echo $encrypt_files;?>
					}
					
				})
                console.log(uuid);

            }
        }
    });
	});
	
    </script>