<!doctype html>
<html>
<head>
<title>Video Uploaded</title>
<script language="JavaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>    

<script type="text/javascript">
var checkInterval = 
setInterval(
	function(){ 
		$.ajax({
			url: "/unprocessed",
			method: "post",
			data:{
				"_token" : "{{ csrf_token() }}"	
			},
			success: function(data){
				if(data.action == "process"){
					clearInterval(checkInterval);
					$.ajax({
						url: "/php_youtube/index.php",
						method: "post",
						data:{
							"path" : data.path	,
							"id" : data.id
						},
						dataType:"JSON",
						success: function(data){
							if (typeof data.youtube_id == "undefined"){
								location.reload();
								
								
							}else{
								$.ajax({
									url: "/file/setProcessed",
									method: "post",
									data:{
										youtube_id :data.youtube_id,
										id:data.id 	,
										"_token" : "{{ csrf_token() }}"
									},
									dataType:"JSON",
									success: function (data) {
										location.reload();
									}
								})
							}
							
							
							
						}
					})
	
				}
				
			}
		})
	}, 
20000);
</script>

</head>
<body>
</body>
</html>
