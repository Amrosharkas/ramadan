<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-globe font-color"></i> Dishes
        </div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a class="btn color pjax-link"  href="{{route('task.init')}}">
                            Add New <i class="fa fa-plus"></i>
                        </a>
                       
                    </div>
                </div>
{{--@reorder-button@--}}

            </div>
        </div>

    </div>
    <form id="search-form">
        {{csrf_field()}}
        
        <div class="form-group"></div>
    </form>

    <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">

        <thead>
        <tr class="tr-head">
            <th valign="middle" class="no-sorting table-checkbox-col">
                <input type="checkbox" class="group-checkable">
            </th>
            <th valign="middle">
                        Name
                    </th>
            <th valign="middle">Category</th>
<th valign="middle">
                        Assigned By</th>
<th valign="middle">Assigned to</th>
<th valign="middle">Event name</th><th valign="middle">
                        Actions
                    </th>

        </tr>
        </thead>
        <tbody>
        @foreach($tasks as $index => $task)
        <tr class="odd gradeX" id="data-row-{{$task->id}}">
           <td valign="middle">
               <input type="hidden" value="{{$task->id}}" class="reorder-vals"/>
               <input type="checkbox" name="items[]" class="table-checkbox" value="{{$task->id}}"/>
           </td>
            <td valign="middle">
                        {{$task->name}}
                    </td>
            <td valign="middle">{{$task->category}}</td>
<td valign="middle">
                        <?php if(isset($task->assigned_by->name)){?>{{$task->assigned_by->name}}<?php ;}?>
                    </td>
<td valign="middle"><?php if(isset($task->user->name)){?>{{$task->user->name}}<?php ;}?></td><td valign="middle">{{$task->event->name}}</td><td valign="middle">
                        <a href="{{route('task.dishes_1Edit',['id'=>$task->id])}}" class="btn btn-edit  pjax-link" ><i class="fa fa-edit"></i> Edit</a>
                        <a href="{{route('task.delete',['id'=>$task->id ])}}" data-method="delete" class="btn btn-delete remove-stuff" ><i class="fa fa-remove"></i> Delete</a>
                    </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-globe font-color"></i> Dishes Pending Approval
        </div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group"></div>
                </div>
{{--@reorder-button@--}}

            </div>
        </div>

    </div>
    <form id="search-form">
        {{csrf_field()}}
        
        <div class="form-group"></div>
    </form>

    <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">

        <thead>
        <tr class="tr-head">
            <th valign="middle">Dish</th>
            <th valign="middle">Event</th>
            <th valign="middle">Assigned to</th>
          <th valign="middle">
                        Assigned By
                    </th>
          <th valign="middle">
                        Actions
                    </th>
</tr>
        </thead>
        <tbody>
        @foreach($pending_tasks as $index => $task)
        <tr class="odd gradeX" id="data-row-{{$task->id}}">
           <td valign="middle">{{$task->name}}</td>
           <td valign="middle">
           <?php if(isset($task->event->name)){?>
           {{$task->event->name}}
           <?php ;}?>
           
           </td>
           <td valign="middle">
           <?php if(isset($task->user->name)){?>
           {{ $task->user->name }}
           <?php ;}?>
           </td>
          <td valign="middle">
          <?php if(isset($task->assigned_by->name)){?>
                        {{$task->assigned_by->name}}
                        <?php ;}?>
                    </td>
          <td valign="middle">
            @if($task->confirmed == "")
  <a href="{{route('event_user.reply_task',['id'=>$task->id , 'reply' => 1 ])}}" data-method="get" class="btn btn-success confirm_action" ><i class="fa fa-check"></i> Accept</a>
            <a href="{{route('event_user.reply_task',['id'=>$task->id , 'reply' => 0 ])}}" data-method="get" class="btn btn-delete confirm_action" ><i class="fa fa-remove"></i> Reject</a>
            @endif
          </td>
</tr>
        @endforeach
    </table>
</div>
