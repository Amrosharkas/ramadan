@if(count($event_users) != 0)
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-globe font-color"></i> Pending Invitations
      </div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">

{{--@reorder-button@--}}

            </div>
        </div>

    </div>
    <form id="search-form">
        {{csrf_field()}}
        
        <div class="form-group"></div>
    </form>

    <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">

        <thead>
        <tr class="tr-head">
            <th valign="middle">Event</th>
            <th valign="middle">Location</th>
            <th valign="middle">Timing</th>
          <th valign="middle">
                        Invited By
                    </th>
<th valign="middle">
                        Actions
                    </th>
</tr>
        </thead>
        <tbody>
        @foreach($event_users as $index => $event_user)
        <tr class="odd gradeX" id="data-row-{{$event_user->id}}">
           <td valign="middle"><a href="{{route('event.event_details',['event_id' => $event_user->event->id])}}" > {{$event_user->event->name}}</a></td>
           <td valign="middle">{{$event_user->event->location}}</td>
           <td valign="middle">
           {{ Carbon\Carbon::parse($event_user->event->start_time)->format('l d M Y h:i a') }} - {{ Carbon\Carbon::parse($event_user->event->end_time)->format('h:i a') }}
           </td>
          <td valign="middle">
                        {{$event_user->getInviter->name}}
                    </td>
<td valign="middle">
<a href="{{route('event_user.reply',['id'=>$event_user->id , 'reply' => 'Accepted' ])}}" data-method="get" class="btn btn-success confirm_action" ><i class="fa fa-check"></i> Accept</a>
                        <a href="{{route('event_user.reply',['id'=>$event_user->id , 'reply' => 'Rejected' ])}}" data-method="get" class="btn btn-delete confirm_action" ><i class="fa fa-remove"></i> Reject</a>
                    </td>
</tr>
        @endforeach
    </table>
</div>
@endif
@if(count($user_events) != 0)
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-globe font-color"></i> Your Events
      </div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">

{{--@reorder-button@--}}

            </div>
        </div>

    </div>
  <form id="search-form">
        {{csrf_field()}}
        
        <div class="form-group"></div>
    </form>

    <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">

        <thead>
        <tr class="tr-head">
            <th valign="middle">Event</th>
            <th valign="middle">Location</th>
            <th valign="middle">Timing</th>
          <th valign="middle">
                        Invited By
                    </th>
          <th valign="middle">Status</th>
<th valign="middle">
                        Actions
                    </th>
</tr>
        </thead>
        <tbody>
        @foreach($user_events as $index => $event_user)
        <tr class="odd gradeX" id="data-row-{{$event_user->id}}">
           <td valign="middle">
<a href="{{route('event.event_details',['event_id' => $event_user->event->id])}}" >
           {{$event_user->event->name}}
</a> 
           </td>
           <td valign="middle">{{$event_user->event->location}}</td>
           <td valign="middle">
           {{ Carbon\Carbon::parse($event_user->event->start_time)->format('l d M Y h:i a') }} - {{ Carbon\Carbon::parse($event_user->event->end_time)->format('h:i a') }}
           </td>
          <td valign="middle">
                      <?php if(isset($event_user->getInviter->name)){?>  {{$event_user->getInviter->name}}<?php ;}?>
                    </td>
          <td valign="middle">{{$event_user->status}}</td>
<td valign="middle">
@if($event_user->status == "Rejected")
<a href="{{route('event_user.reply',['id'=>$event_user->id , 'reply' => 'Accepted' ])}}" data-method="get" class="btn btn-success confirm_action" ><i class="fa fa-check"></i> Changed My Mind</a>
@endif
@if($event_user->status == "Accepted")
                        <a href="{{route('event_user.reply',['id'=>$event_user->id , 'reply' => 'Rejected' ])}}" data-method="get" class="btn btn-delete confirm_action" ><i class="fa fa-remove"></i> Leave Event</a>
                        @endif
                    </td>
</tr>
        @endforeach
    </table>
</div>
@endif

@if(count($my_tasks) != 0)
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-color">
            <i class="fa fa-globe font-color"></i> Your Dishes
      </div>

    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">

{{--@reorder-button@--}}

            </div>
        </div>

    </div>
  <form id="search-form">
        {{csrf_field()}}
        
        <div class="form-group"></div>
    </form>

    <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">

        <thead>
        <tr class="tr-head">
            <th valign="middle">Dish</th>
            <th valign="middle">Event</th>
            <th valign="middle">Timing</th>
          <th valign="middle">
                        Assigned By
                    </th>
          <th valign="middle">Approved By</th>
          <th valign="middle">
                        Actions
                    </th>
</tr>
        </thead>
        <tbody>
        @foreach($my_tasks as $index => $task)
        <tr class="odd gradeX" id="data-row-{{$task->id}}">
           <td valign="middle">{{$task->name}} <a href="{{route('task.comments',['task_id' => $task->id])}}" class="popup" ><i class="fa fa-edit font-color"></i> Comments {{$task->commentsCount->count()}}</a></td>
           <td valign="middle">{{$task->event->name}}</td>
           <td valign="middle">
           {{ Carbon\Carbon::parse($task->event->start_time)->format('l d M Y h:i a') }} - {{ Carbon\Carbon::parse($task->event->end_time)->format('h:i a') }}
           </td>
          <td valign="middle">
                        {{$task->assigned_by->name}}
                    </td>
          <td valign="middle">
          @if($task->assigned_by_id == $task->user_id)
          Organizer
          @else
          {{$task->user->name}}
          @endif
          </td>
          <td valign="middle">
            @if($task->confirmed == "" && $task->assigned_by_id != $task->user_id)
  <a href="{{route('event_user.reply_task',['id'=>$task->id , 'reply' => 1 ])}}" data-method="get" class="btn btn-success confirm_action" ><i class="fa fa-check"></i> Accept</a>
            <a href="{{route('event_user.reply_task',['id'=>$task->id , 'reply' => 0 ])}}" data-method="get" class="btn btn-delete confirm_action" ><i class="fa fa-remove"></i> Reject</a>
            @endif
          </td>
</tr>
        @endforeach
    </table>
</div>
@endif
