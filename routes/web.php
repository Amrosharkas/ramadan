<?php
Route::Auth();
Route::get('/logout','Auth\LoginController@logout');
Route::get('/','UserController@index');
Route::get('/handle_youtube','youtubeController@index');
Route::post('/unprocessed','youtubeController@unProcessed');
Route::post('/file/setProcessed','youtubeController@setProcessed');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group( ['middleware' => ['auth','authorize'],'roles' => ['Super'],'prefix' => "user", 'as' => "user."], function () {
    
    Route::get('/users', 'UserController@usersIndex')->name('usersindex');
    Route::get('{id}/users_1Edit', 'UserController@users_1Edit')->name('users_1Edit');
    Route::post('{id}/users_1Edit', 'UserController@users_1Update')->name('users_1Edit');
    Route::get('init', 'UserController@init')->name('init');
    Route::delete('{id}/delete', 'UserController@delete')->name('delete');
//--routes@@user

});
Route::group( ['middleware' => ['auth','can:super_user'],'prefix' => "event", 'as' => "event."], function () {
                    Route::get('/events', 'EventController@eventsIndex')->name('eventsindex');
    Route::get('{id}/events_1Edit', 'EventController@events_1Edit')->name('events_1Edit');
    Route::post('{id}/events_1Edit', 'EventController@events_1Update')->name('events_1Edit');
    Route::get('init', 'EventController@init')->name('init');
    Route::delete('{id}/delete', 'EventController@delete')->name('delete');
//--routes@@event

});
Route::group( ['middleware' => ['auth'],'prefix' => "event", 'as' => "event."], function () {
                    
    Route::get('{id}/event_details', 'EventController@event_details')->name('event_details');
    Route::get('{memory_id}/show_memory', 'EventController@show_memory')->name('show_memory');
    
//--routes@@event

});
Route::group(['middleware' => ['auth','can:super_user'],'prefix' => "task", 'as' => "task."], function () {
                    Route::get('/dishes', 'TaskController@dishesIndex')->name('dishesindex');
    Route::get('{id}/dishes_1Edit', 'TaskController@dishes_1Edit')->name('dishes_1Edit');
    Route::post('{id}/dishes_1Edit', 'TaskController@dishes_1Update')->name('dishes_1Edit');
    Route::get('init', 'TaskController@init')->name('init');
    Route::delete('{id}/delete', 'TaskController@delete')->name('delete');
//--routes@@task

});
Route::group(['middleware' => ['auth'],'prefix' => "task", 'as' => "task."], function () {
    Route::get('/pickup/{task_id}/{user_id}', 'TaskController@pickup')->name('pickup');
    Route::get('/comments/{task_id}', 'TaskController@comments')->name('comments');
//--routes@@task

});
Route::group( ['middleware' => ['auth','can:super_user'],'prefix' => "group", 'as' => "group."], function () {
                    Route::get('/groups', 'GroupController@groupsIndex')->name('groupsindex');
    Route::get('{id}/groups_1Edit', 'GroupController@groups_1Edit')->name('groups_1Edit');
    Route::post('{id}/groups_1Edit', 'GroupController@groups_1Update')->name('groups_1Edit');
    Route::get('init', 'GroupController@init')->name('init');
    Route::delete('{id}/delete', 'GroupController@delete')->name('delete');
//--routes@@group

});



Route::group( ['middleware' => ['auth'],'prefix' => "event_user", 'as' => "event_user."], function () {
                    Route::get('/invitations', 'EventUserController@invitationsIndex')->name('invitationsindex');
                    Route::get('/reply/{id}/{reply}', 'EventUserController@respondToEventInvitation')->name('reply');
                    Route::get('/reply_task/{id}/{reply}', 'EventUserController@respondToTaskInvitation')->name('reply_task');

//--routes@@event_user

});
Route::group( ['middleware' => ['auth'],'prefix' => "comment", 'as' => "comment."], function () {
                    Route::post('/do_comment', 'EventController@do_comment')->name('do_comment');
                    

//--routes@@event_user

});
Route::group( ['middleware' => ['auth'],'prefix' => "files", 'as' => "files."], function () {
                    Route::get('/add', 'memoryController@add')->name('add');
                    Route::post('/endpoint', 'fileController@handle')->name('endpoint');
                    Route::post('/save_file', 'fileController@saveFile')->name('save_file');
                    Route::delete('/endpoint/{file}', 'fileController@handle')->name('delete_endpoint');
                    Route::post('/get_file_info', 'fileController@getFileInfo')->name('get_file_info');
                    
    
//--routes@@files

});
//--routes--





