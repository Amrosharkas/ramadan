<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbuseCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abuse_cases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reported_by')->nullable();
            $table->string('status')->nullable();
            $table->integer('reviewed_by')->nullable();
            $table->text('description')->nullable();
            $table->integer('user_reported')->nullable();
            $table->integer('file_id')->nullable();
            $table->integer('comment_id')->nullable();
            $table->integer('admin_show')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abuse_cases');
    }
}
