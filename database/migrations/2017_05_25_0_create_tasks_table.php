<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category')->nullable();
            $table->string('name')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('assigned_by_id')->nullable();
            $table->string('description')->nullable();
            $table->integer('confirmed')->nullable();
            $table->date('deadline')->nullable();
            $table->string('type')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('event_id')->nullable();
            $table->integer('task_image')->nullable();
            $table->integer('admin_show')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
