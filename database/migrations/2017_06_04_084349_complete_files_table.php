<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompleteFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('files', function ($table) {
            $table->text('hash')->nullable();
            $table->integer('processed')->default(0);
            $table->text('youtube_id')->nullable();
            $table->text('file_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function($table) {
            $table->dropColumn('hash');
            $table->dropColumn('processed');
            $table->dropColumn('youtube_id');
            $table->dropColumn('file_type');
        });
    }
}
