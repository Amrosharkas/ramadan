<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'Mohamed Tarek',
            'email'=>'mtarek@arabiclocalizer.com',
            'password'=>bcrypt('123456'),
            'admin_show'=>1,
            'user_type'=>'User'
            ]);


    }
}
